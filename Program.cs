﻿using System;
using System.Collections.Generic;

namespace MArrayConsTwo
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] statues = new int[4] {6, 2, 3, 9};
            int newStatues = makeArrayConsecutive2(statues);
            System.Console.WriteLine(newStatues);
        }
        public static int makeArrayConsecutive2(int[] statues) {
            //int n = 0;
            List<int> newStatues = new List<int>();

            for (int i = 0; i < statues.Length; i++)
            {
                newStatues.Add(statues[i]);
            }

            newStatues.Sort();
            int difference = 0;
            for (int i = 0; i < newStatues.Count - 1; i++)
            {
                if(newStatues[i + 1] - newStatues[i] > 1 ){
                    for (int j = newStatues[i] + 1; j < newStatues[i + 1]; j++)
                    {
                        difference++;
                    }
                }

                // if(newStatues[i] > newStatues[i + 1]){
                //     int temp = newStatues[i];
                //     newStatues[i] = newStatues[i + 1];
                //     newStatues[i + 1] = temp;
                // }
            }
            return difference;
    
        }

    
    
    }
}
